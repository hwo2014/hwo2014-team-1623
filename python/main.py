import json
import socket
import sys
import pprint

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
	self.currentPiece = 0
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        if self.get_current_piece(data) != self.currentPiece:
		print("Current piece is %d" % self.get_current_piece(data))
		#if not self.crashedPiece:
			#self.update_line(self.currentPiece-1, 1)
			
		self.crashedPiece = False
	self.currentPiece = self.get_current_piece(data)
	self.throttle(float(self.sourceLines[self.currentPiece]) / 1000)	

    def on_crash(self, data):
        print("Someone crashed")
	self.crashedPiece = True
	self.update_line(self.currentPiece, -5)
	self.update_line(self.currentPiece-1, -5)
	self.update_line(self.currentPiece-2, -2)
	self.update_line(self.currentPiece-3, -2)
	self.update_line(self.currentPiece-4, -2)
	self.update_line(self.currentPiece-5, -2)
	self.update_line(self.currentPiece-6, -2)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
	self.source.seek(0)
	self.source.truncate()
	for each in self.sourceLines:
		self.source.write("%s\n" % each)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
	print("Your color is %s" % format(data["color"]))

    def on_game_init(self, data):
	print("Current track is %s" % format(data["race"]["track"]["name"]))
	print("Track consists of %s pieces" % len(data["race"]["track"]["pieces"]))	
	self.source = open(str(data["race"]["track"]["name"]) + '.txt', 'r+')
	self.sourceLines = [int(line.strip()) for line in self.source]
	self.crashedPiece = False

    def update_line(self, number, difference):
	if self.sourceLines[number] + difference <= 100 and self.sourceLines[number] + difference >= 0:
		self.sourceLines[number] += difference

    def get_current_piece(self, data):
	return int(format(data[0]["piecePosition"]["pieceIndex"]))
	
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
	    'yourCar': self.on_your_car,
	    'gameInit': self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
